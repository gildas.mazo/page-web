<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link rel="stylesheet" href="style.css" />
<title>Gildas Mazo</title>
</head>

<body>

<header>
<?php include("header.html"); ?>
</header>

<table width="100%">
  <tr>
    <td style="width:33%">
      <h3>Address and contact</h3>
      MaIAGE, INRAE, Université Paris-Saclay<br>
      78350, Jouy-en-Josas, France<br>
      gildas (dot) mazo (at) inrae (dot) fr<br>

      <h3>Research interests</h3>
      <ul>
	<li>copulas</li>
	<li>mixture models</li>
	<li>extreme value theory</li>
	<li>semiparametric methods</li>
	<li>sensitivity analysis</li>
      </ul>
    </td>

    <td width="66%">

      <h3>Profile</h3>
      <p>I am a researcher in statistics at 
	<a href="http://maiage.jouy.inra.fr/">MaIAGE</a>,
	INRAE and Université Paris-Saclay, France. Prior to INRAE, I held postdoc positions at
	<a href="http://team.inria.fr/mistis/">MISTIS</a>,
	Inria Grenoble Rhone-Alpes, France (2016-2017) and at 
	<a href="http://uclouvain.be/fr/node/9330">ISBA</a>, Université Catholique de Louvain, Belgium (2015-2016). I completed a PhD in statistics at MISTIS, Inria Grenoble Rhone-Alpes and
	<a href="http://www-ljk.imag.fr/">Laboratoire Jean Kuntzmann</a>,
	Université de Grenoble Alpes, France (2014).</p>

      <h3>Research interests</h3>
      <p>My main research interests include sensitivity analysis and copula methods.</p>

      <h4>Copulas</h4>
      <p>Copula methods provide a convenient way to construct joint distributions and  design sequential inference procedures. The concept of copula is quite related to that of interdependence of random variables and hence is useful in many areas, such as extreme value theory or multivariate mixture models. Due to the split between the marginal effects and the dependence structure, copula-based models can be subjected to semiparametric methods. My current research interests lie on copula methods for (i)  mixture models and (ii) discrete data.</p>
      <p>It is possible to exploit the concept of copulas to build flexible mixture models for non-elliptical data but research questions arise such as, for instance, the identifiability of those models or the ability to design computationally efficient parametric or semiparametric methods.</p>
      <p>In the case of discrete data, copulas lead to computationally difficult inference problems and their interpretation is not as straightforward as in the case of continuous data. Nonetheless, copulas remain a convenient tool to build joint distributions and hence it worth the effort to know whether the difficulties raised above can be efficiently overcome. To give an example of application, RNAseq data could benefit from the development of copula methods.</p>

      <h4>Sensitivity analysis</h4>
      <p>Sensitivity analysis permits to study how a computer model is sensitive to its inputs. One way to do such an analysis is to draw at random the inputs and examine the variability of the output. For instance, Sobol indices quantify the proportion of the variance that could be erased if some inputs could be fixed to their true values. My current research interests lie on stochastic computer models, that is, those that can return different outputs given the same input. In this setting, open questions arise. What are good sensitivity indices? How to build good estimators that achieve a good tradeoff between  statistical efficiency and computational efficiency? These problems are even more complicated when the output is of complex nature, such as multidimensional objects or functions. To give an example of an application, epidemic models have functional and stochastic outputs.</p>
    </td>
  </tr>
</table>

<br>

<footer>
<?php include("footer.html"); ?>
</footer>


<!--Collect the visiting IP addresses-->	
<?php
$myfile = fopen("addresses.txt", "a") ;
$txt = date("c")." ".$_SERVER["REMOTE_ADDR"]."\n";
fwrite($myfile, $txt);
fclose($myfile);
?> 


</body>
</html>
