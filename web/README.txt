
MODE D'EMPLOI POUR METTRE A JOUR MA PAGE WEB

1. Faire les modifications voulues directement dans les fichiers
   concernés html.
2. Si la mise à jour concerne la bibliographie, suivre les 
   instructions données dans le fichier 'publications.html'.
3. Si un nouveau fichier (html) est créé, ajouter la balise Jekyll au
   début (voir les fichiers existants pour un exemple).
4. "Construire" le site avec Jekyll.
5. Faire la mise à jour sur migale.

Pour construire le site, faire à la racine
   jekyll build
Le dossier _site/ est alors mis à jour avec les fichiers créés
par Jekyll. Pour vérifier que tout fonctionne, faire 
   jekyll serve
On peut alors visualiser le site à une addresse locale donnée en
sortie par Jekyll. 

Pour  la mise à jour sur Migale, faire
   rsync -av /home/gmazo/projets-git/page-web/web/_site/
   gmazo@front.migale.inrae.fr:/projet/web/htdocs/migale/gmazo/public_html/

ATTENTION ! Les permissions d'accès aux fichiers en mode lecture
doivent être données à tous les utilisateurs (chmod o+r
nomfichier.html) sur le serveur. Cela peut être fait manuellement
depuis le serveur ou localement avec préservation des droits lors du
transfert des fichiers (voir la documentation de rsync).

REMARQUE : on a pas besoin des fichiers gemfile et Gemfile pour
construire son site avec Jekyll. Ces fichiers sont nécessaires si on
veut héberger sa page sur forgemia (voir ci-dessous).

NOTE (PHP) : Parfois les scripts PHP ne sont pas exécutés sur les serveurs
d'hébergement de page web.  En gros, il y a deux types de sites web
[2]. Les sites web statiques et les sites web dynamiques. Les sites
web dynamiques, qui permettent l'exécution de scripts PHP, ont besoin
de serveurs plus complexes et c'est pour cela que ni Gitlab Pages ni
le disque internet de la PLM ne les autorisent. Néanmoins, il est
quand même possible de coder son site comme si c'était un site
dynamique et de le transformer en site statique grâce à un outil qui
s'appelle "Static Site Generator" (SSG) [2]. En gros, il suffit de
remplacer include(header.php) par {% include header.html %}.

NOTE (HEBERGEURS) : J'ai alors considéré deux
alternatives pour héberger mon site web: les services numériques de la
PLM du CNRS et Gitlab Pages [1] sur forgemia.
La PLM propose deux services pour héberger des sites web. D'abord il y
a le "disque internet", mais il ne permet pas d'exécuter des scripts
PHP. Ensuite il y a un hébergement web mais il est écrit qu'il n'est
pas fait pour héberger une page web perso. De son côté Gitlab Pages ne
permet pas d'exécuter des scipts PHP.
Pour utiliser Gitlab Pages avec forgemia, voir [3]. Pour un tutoriel
de Gitlab, voir [4]. Remarque : Il faut installer ruby-bundler. Dans
[4], c'est le SSG Jekyll qui est utilisé. Jekyll utilise la syntaxe de
Liquid [5]. Le quickstart de Jekyll est bien fait [6].

REFERENCES
[1]
https://forgemia.inra.fr/adminforgemia/doc-public/-/wikis/gitlab-pages
[2]
https://about.gitlab.com/blog/2016/06/10/ssg-overview-gitlab-pages-part-2/ 
[3] https://forgemia.inra.fr/adminforgemia/doc-public/-/wikis/gitlab-pages
[4]
https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html
[5] https://shopify.github.io/liquid/basics/introduction/
[6] https://jekyllrb.com/docs/
-------------------------------------
