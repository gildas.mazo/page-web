Le fichier principal est "cv.tex". Ce fichier appelle une base de
données Bibtex (collection de fichier ".bib"). 
 - communications ecrites : publis, preprints, chaplivres, raprecherche
 - communications orales : posters, seminaires, conferences

À l'exception des posters ("posters.bib") et des séminaires
("seminaires.bib"), toutes les communications orales sont dans le
fichier "conferences.bib". J'ai choisi de ne pas faire de distinctions
entre les conférences nationales et internationales, entre les
"petites" et les "grandes" conférences (la raison est que parfois je
ne sais pas dans quelle catégorie ranger la conférence en question et
je n'ai pas envie de me prendre la tête avec ça). En revanche, j'ai
envie de distinguer les présentations effectivement données par moi de
celles données par mes co-auteurs (étudiants ou non). Pour l'instant
cette distinction est faite au sein du fichier "conferences.bib": les
co-auteurs sont à la fin. De vielles conférences peuvent être trouvées
dans le fichier "oldconferences.tex".

Pour les séminaires, une liste est maintenue mais n'apparait pas dans
mon CV par défaut. La raison est que je ne sais pas ce que je dois
mettre dans le champ "auteur". En effet, les séminaires sont pour moi
souvent l'occasion de présenter un point de vue personel sur un sujet,
ou de faire une revue d'un ensemble de travaux antérieurs impliquant
potentiellements plusieurs groupes de co-auteurs. Je pourrais mettre
mes séminaires sans rien mettre dans ce champ, mais ça reste
bizarre. J'ai donc décidé de procéder au cas par cas et de n'ajouter
ma liste de séminaire qu'à l'occasion de demandes ou besoins
spécifiques. 

Some hints for writing a CV are given in the book of Rosei and
Johnston, "Survival skills for scientists", p. 153. See also the CV
of D. Knuth for an example. (Given somewhere as a pdf file.) 

28 JUILLET 2023.
   CV à jour.

12 JANVIER 2023. 
   CV à jour.


