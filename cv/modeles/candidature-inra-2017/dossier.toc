\select@language {french}
\contentsline {chapter}{\numberline {1}Curriculum Vitae}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Situation professionnelle actuelle}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Exp\IeC {\'e}riences professionnelles ant\IeC {\'e}rieures}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Encadrement d'activit\IeC {\'e}s de recherche}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Responsabilit\IeC {\'e}s collectives}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Collaborations, mobilit\IeC {\'e}}{3}{section.1.5}
\contentsline {section}{\numberline {1.6}Enseignement}{3}{section.1.6}
\contentsline {section}{\numberline {1.7}Diffusion de l'information scientifique}{4}{section.1.7}
\contentsline {chapter}{\numberline {2}Rapport sur les travaux de recherche}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Introduction}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Copules}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Th\IeC {\'e}orie des valeurs extr\IeC {\^e}mes}{6}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Mod\IeC {\`e}les de m\IeC {\'e}lange}{7}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Contributions}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Construction et estimation de copules multivari\IeC {\'e}es}{7}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Construction de r\IeC {\'e}seaux de Markov avec transitions extr\IeC {\^e}mes}{8}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Construction et estimation de m\IeC {\'e}langes non-param\IeC {\'e}triques multivari\IeC {\'e}s}{8}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Impl\IeC {\'e}mentations, applications et transfert}{9}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Impl\IeC {\'e}mentations et applications}{9}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Transfert}{9}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Insertion des comp\IeC {\'e}tences}{10}{section.2.4}
\contentsline {chapter}{\numberline {3}Liste de publications}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Revues internationales}{13}{section.3.1}
\contentsline {section}{\numberline {3.2}Conf\IeC {\'e}rences internationales}{13}{section.3.2}
\contentsline {section}{\numberline {3.3}Chapitre de livre}{14}{section.3.3}
\contentsline {section}{\numberline {3.4}Conf\IeC {\'e}rence nationale}{14}{section.3.4}
\contentsline {section}{\numberline {3.5}Rapport de recherche}{14}{section.3.5}
\contentsline {section}{\numberline {3.6}Logiciels}{14}{section.3.6}
