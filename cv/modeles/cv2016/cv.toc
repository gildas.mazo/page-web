\babel@toc {french}{}
\contentsline {section}{\numberline {0.1}Situation professionnelle actuelle}{1}{section.0.1}%
\contentsline {section}{\numberline {0.2}Expériences professionnelles antérieures}{1}{section.0.2}%
\contentsline {section}{\numberline {0.3}Encadrement d'activités de recherche}{1}{section.0.3}%
\contentsline {section}{\numberline {0.4}Responsabilités collectives}{2}{section.0.4}%
\contentsline {section}{\numberline {0.5}Collaborations, mobilité}{2}{section.0.5}%
\contentsline {section}{\numberline {0.6}Enseignement}{3}{section.0.6}%
\contentsline {section}{\numberline {0.7}Diffusion de l'information scientifique}{3}{section.0.7}%
\contentsline {section}{\numberline {0.8}Liste de publications}{3}{section.0.8}%
\contentsline {subsection}{\numberline {0.8.1}Revues internationales}{3}{subsection.0.8.1}%
\contentsline {section}{\numberline {0.9}Conférences internationales}{4}{section.0.9}%
\contentsline {section}{\numberline {0.10}Chapitre de livre}{4}{section.0.10}%
\contentsline {section}{\numberline {0.11}Conférence nationale}{4}{section.0.11}%
\contentsline {section}{\numberline {0.12}Rapport de recherche}{5}{section.0.12}%
\contentsline {section}{\numberline {0.13}Logiciels}{5}{section.0.13}%
